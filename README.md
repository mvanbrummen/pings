# Pings

[![build status](https://gitlab.com/mvanbrummen/pings/badges/master/build.svg)](https://gitlab.com/mvanbrummen/pings/commits/master)

A simple API for time clock pings written for the [Tanda backend challenge.](https://github.com/TandaHQ/work-samples/tree/master/pings%20(backend))

